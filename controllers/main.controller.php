<?php
class MainController extends Controller{

    public function index(){
        $data = $this->model->getData();
        $this->view->show($data);
    }
}