<?php
class AuthController extends Controller{

    public function index(){}
    public function login(){
        $data = $this->model->getData();
        $this->view->show($data);
    }

    public function logout(){
        $this->model->logout();
    }
}