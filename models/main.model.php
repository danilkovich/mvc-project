<?php
class MainModel extends Model{
    private function index(){
        $query = "select image_title,url from logos";
        $data = Db::query($query);
        shuffle($data);
        return $data;
    }

    public function getData(){
        $data = array_merge(
            $this->getSiteName(),
            $this->getLogo(),
            $this->getFooter()
        );

        $data['content'] = $this->{$this->params['action']}();
        $data['header'] =  $this->getHeader();
        $data['main_menu'] = $this->getMainMenu();
        $data['bottom_menu'] = $this->getBottomMenu();
        $data['sidebar'] = $this->getSidebar();
        $data['banners'] = $this->getBanners();

        return $data;
    }
}