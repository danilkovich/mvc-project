<?php
class AuthModel extends Model{

    protected function login(){
        $data = array();
        if($_POST['login'] && $_POST['password']){
            $password = md5($_POST['password']);

            $login = Db::real_escape_string($_POST['login']);
            $query = "select password from users where login = '$login'";
            $data = $this->fetchSingleRow(Db::query($query));

            if($data) {
                if ($password == $data){
                    session_start();
                    $_SESSION['admin'] = rand();
                    header("Location: http://{$_SERVER['HTTP_HOST']}/admin/index.php");
                    exit;
                }
            }
            $data['warning'] = "Неверное сочетание логин/пароль.";
        }

        return $data;
    }

    public function logout(){
        session_start();
        session_unset();
        header("Location: http://{$_SERVER['HTTP_HOST']}/index.php");
        exit;
    }

    public function getData(){
        $data = $this->{$this->params['action']}();
        return $data;
    }
}