<?php
abstract class Model{
    protected $params;

    public abstract function getData();

    public function __construct($params){
        $this->params = $params;
    }

    protected function getMainMenu(){

        $menu = array(
            'Головна' => array(
                'url' => '/?',
                'controller' => 'main',
                'action' => 'index',
                'is_active' => false,
            ),
            'Про нас' => array(
                'url' => '/?',
                'controller' => 'pages',
                'action' => 'about_us',
                'is_active' => false,
                'drop_down' => array(
                    'Рада Асоціації' => array(
                        'url' => '/?',
                        'controller' => 'pages',
                        'action' => 'about_us',
                        'alias' => 'council',
                    ),
                    'Учасники' => array(
                        'url' => '/?',
                        'controller' => 'pages',
                        'action' => 'about_us',
                        'alias' => 'members',
                    ),
                    'Статут' => array(
                        'url' => '/?',
                        'controller' => 'regulation',
                        'action' => 'index',
                        'bind' => array(
                            'controller' => 'pages',
                            'action' => 'about_us',
                        ),
                    ),
                    'Співробітники' => array(
                        'url' => '/?',
                        'controller' => 'pages',
                        'action' => 'about_us',
                        'alias' => 'collaborators',
                    ),
                    'Відділення' => array(
                        'url' => '/?',
                        'controller' => 'pages',
                        'action' => 'about_us',
                        'alias' => 'departments',
                    ),
                ),
            ),
            'Діяльність' => array(
                'url' => '/?',
                'controller' => 'articles',
                'action' => 'activity',
                'is_active' => false,
                'drop_down' => array(
                    'Семінари' => array(
                        'url' => '/?',
                        'controller' => 'articles',
                        'action' => 'activity',
                        'category' => 'seminars',
                    ),
                    'Засідання' => array(
                        'url' => '/?',
                        'controller' => 'articles',
                        'action' => 'activity',
                        'category' => 'meetings',
                    ),
                    'Загальні збори' => array(
                        'url' => '/?',
                        'controller' => 'articles',
                        'action' => 'activity',
                        'category' => 'gathering',
                    ),
                    'Конференці' => array(
                        'url' => '/?',
                        'controller' => 'articles',
                        'action' => 'activity',
                        'category' => 'conference',
                    ),
                ),
            ),
            'Новини' => array(
                'url' => '/?',
                'controller' => 'articles',
                'action' => 'news',
                'is_active' => false,
            ),
            'Правова База' => array(
                'url' => '/?',
                'controller' => 'legislative_base',
                'action' => 'index',
                'is_active' => false,
            ),
            'Контакти' => array(
                'url' => '/?',
                'controller' => 'pages',
                'action' => 'contacts',
                'is_active' => false,
            ),
            'Форум' => array(
                'url' => '/forum/index.php',
                'is_active' => false,
            ),
        );

        $this->setUrl($menu);

        return $menu;
    }

    protected function getBottomMenu(){

        $menu = array(
            "Робочі Групи" => array(
                'url' => '/?',
                'controller' => 'work_groups',
                'action' => 'index',
                'is_active' => false,
            ),
            "Галузеві Видання" => array(
                'url' => '/?',
                'controller' => 'articles',
                'action' => 'journal',
                'is_active' => false,
            ),
            "Партнери" => array(
                'url' => '/?',
                'controller' => 'partners',
                'action' => 'index',
                'is_active' => false,
                'drop_down' => array(
                    'Українські Асоціації' => array(
                        'url' => '/?',
                        'controller' => 'partners',
                        'action' => 'index',
                        'alias' => 'ukrainian',
                    ),
                    'Міжнародні Організації' => array(
                        'url' => '/?',
                        'controller' => 'partners',
                        'action' => 'index',
                        'alias' => 'international',
                    ),
                ),
            ),
            "Водоканали в Цифрах" => array(
                'url' => '/?',
                'controller' => 'articles',
                'action' => 'water_channel_in_num',
                'is_active' => false,

            ),
            "Фотогалерея" => array(
                'url' => '/?',
                'controller' => 'gallery',
                'action' => 'index',
                'is_active' => false,
            ),
        );

        $this->setUrl($menu);

        return $menu;
    }

    protected function setUrl(&$menu){
        $bundle = array();

        foreach($menu as &$value){
            $value['url'] .= http_build_query(array(
                'controller' => $value['controller'],
                'action' => $value['action']));

            if(isset($value['drop_down'])){
                foreach ($value['drop_down'] as &$drop_down) {
                    $url = array_shift($drop_down);

                    if(isset($drop_down['bind'])){
                        $bundle[$drop_down['controller']] = $drop_down['bind'];
                        unset($drop_down['bind']);
                    }

                    $drop_down['url'] = $url.http_build_query($drop_down);
                    unset($drop_down);
                }
            }
            $value['is_active'] = (
                ($this->params['action'] == $value['action']
                    && $this->params['controller'] == $value['controller']
                ) ||
                (array_key_exists($this->params['controller'],$bundle)
                    && $bundle[$this->params['controller']]['action'] == $value['action']
                    && $bundle[$this->params['controller']]['controller'] == $value['controller']
                )
            );
            unset($value);
        }
        unset($menu);
    }

    protected function getSiteName(){
        $query = "select content from common where title='site_name'";
        $data['site_name'] = $this->fetchSingleRow(Db::query($query));
        return $data;
    }

    protected function getLogo(){
        $query = "select content from common where title='logo_title'";
        $data['logo_title'] = $this->fetchSingleRow(Db::query($query));
        return $data;
    }

    protected function getFooter(){
        $query = "select content from common where title='footer'";
        $data['footer'] = $this->fetchSingleRow(Db::query($query));
        return $data;
    }

    protected function getHeader(){
        $query = "select * from articles where category_id in('1','2','3','4') order by date desc, id desc limit 4";
        return Db::query($query);
    }

    protected function getSidebar(){
        $query = "select * from articles where category_id
                  in('5','6','7','8','9','10','11','12','13','14','15','16','17','18','19','20')
                  order by date desc, id desc limit 5";
        return Db::query($query);
    }

    protected function getBanners(){
        $query = "select * from banners order by id";
        $result = Db::query($query);
        $data['sidebar_top'] = $result[0];
        $data['sidebar_bottom'] =  $result[1];
        $data['top'] = array_splice($result,2);
        return $data;
    }

    protected function fetchSingleRow($data){
        if(is_array($data) && count($data) == 1){
            foreach($data as $value){
                $data = $this->fetchSingleRow($value);
            }
        }
        return $data;
    }

    protected function getList($categories = '',$date = '',$limit = 5){
        $append = '';
        if(!empty($categories)){
            $id = $this->parseCategories($categories);
            $append = "where category_id in({$id})";
            $append .= !empty($date) ? "and date='{$date}'" : '';
        }else if(!empty($date)){
            $append = "where date='{$date}'";
        }

            $cur_page = $this->getPage();
        $start = ($cur_page-1) * $limit;
        $order_by = ($this->params['controller'] == 'articles' ? 'order by date desc, id' : 'order by id');
        $query = "select * from {$this->params['controller']} {$append} {$order_by} desc limit {$start},{$limit}";
        $data['content_list'] =  Db::query($query);

        $query = "select count(id) from {$this->params['controller']} {$append}";
        $count = $this->fetchSingleRow(Db::query($query));
        $data['nav_list'] = $this->navList($count,$limit);


        if(Db::getError()){
            die(Db::getError());
        }
        return $data;
    }

    protected function parseCategories($categories){
        if(is_numeric($categories)){
            $id = $categories;
        } else{
            $data = '';
            if(is_array($categories)){
                $data = $categories;
            } else if(is_string($categories)) {
                $query = "select id from {$this->params['controller']}_categories where name in({$categories})";
                $data = Db::query($query);
            }

            $id_arr = array();
            foreach($data as $value){
                $id_arr[] =  $value['id'];
            }
            $id = implode(',',$id_arr);
        }
        return $id;
    }

    private function getCategories($categories){
        return isset($this->params['category']) ? "'".$this->params['category']."'" : $categories;
    }

    private function getDate(){
        return isset($this->params['date']) ? $this->params['date'] : '';
    }

    protected function navList($count,$limit){
        $pages = $this->countPages($count,$limit);
        if($pages > 1){
            $nav_list = array();
            $cur_page = $this->getPage();
            $url_query = parse_url($_SERVER['REQUEST_URI'],PHP_URL_QUERY);
            $parse_url = isset($this->params['page']) ? substr($url_query,0,strrpos($url_query,'&')) : $url_query;
            $url ='http://'.$_SERVER['HTTP_HOST'].'/?'.$parse_url;

            $prev = ($cur_page <= 1) ? '1' : ($cur_page -1);
            $next = ($cur_page >= $pages) ? $pages : ($cur_page +1);
            $start_page = (($cur_page - 3) > 2) ? ($cur_page - 3) : 2;
            $end_page = (($cur_page + 2) < ($pages - 1)) ? ($cur_page + 2) : ($pages - 1);

            if($cur_page > 1){
                $nav_list[] = array(
                    'title' => '<',
                    'url' => $url.'&page='.$prev,
                    'move' => true,
                );
            }

            if($pages > 5) {
                $nav_list[] = array(
                    'title' => '1',
                    'url' => $url.'&page=1',
                    'is_active' => ($cur_page == 1) ? true : false,
                );

                if($start_page != 2){
                    $nav_list[] = array(
                        'title' => '...',
                        'space' => true,
                    );
                }

                for ($i = $start_page; $i <= $end_page; $i++) {
                    $nav_list[] = array(
                        'title' => $i,
                        'url' => $url.'&page='.$i,
                        'is_active' => ($cur_page == $i) ? true : false,
                    );
                }

                if($end_page != $pages - 1){
                    $nav_list[] = array(
                        'title' => '...',
                        'space' => true,
                    );
                }

                $nav_list[] = array(
                    'title' => $pages,
                    'url' => $url.'&page='.$pages,
                    'is_active' => ($cur_page == $pages) ? true : false,
                );
            } else {
                for ($i = 1; $i <= $pages; $i++) {
                    $nav_list[] = array(
                        'title' => $i,
                        'url' => $url.'&page='.$i,
                        'is_active' => ($cur_page == $i) ? true : false,
                    );
                }
            }
            if($cur_page < $pages){
                $nav_list[] = array(
                    'title' => '>',
                    'url' => $url.'&page='.$next,
                    'move' => true,
                );
            }
            return $nav_list;
        }
        return null;
    }

    protected function countPages($elements,$limit){
        $pages = ($elements % $limit == 0)
            ? $elements/$limit
            : floor($elements/$limit) + 1;
        return $pages;
    }

    protected function getPage(){
        return isset($this->params['page']) ? $this->params['page'] : 1;
    }

    protected function ksortTree( &$array )
    {
        if (!is_array($array)) {
            return false;
        }

        ksort($array);
        foreach ($array as $k=>$v) {
            $this->ksortTree($array[$k]);
        }
        return true;
    }

    protected function krsortTree( &$array )
    {
        if (!is_array($array)) {
            return false;
        }

        krsort($array);
        foreach ($array as $k=>$v) {
            $this->krsortTree($array[$k]);
        }
        return true;
    }
}
