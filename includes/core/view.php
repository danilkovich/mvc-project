<?php
abstract class View{
    protected $params;
    protected $alias;
    protected $page_template;
    protected $index_template;
    protected $logo_template;
    protected $social_template;
    protected $main_menu_template;
    protected $bottom_menu_template;
    protected $banner_top_template;
    protected $footer_template;

    public abstract function show($data);

    public function __construct($params){
        $this->init($params);
    }

    protected function init($params){
        if(empty($this->alias)){
            $this->alias = !empty($params['alias']) ? $params['alias'] : $params['action'];
        }
        $this->params = $params;
        $this->page_template = $this->validateUrl(TEMPLATE_PATH.$this->params['controller'].'/'.$this->alias.'.ctp');
        $this->index_template = $this->validateUrl(TEMPLATE_PATH.'index.ctp');
        $this->logo_template = $this->validateUrl(TEMPLATE_PATH.'helpers/logo.ctp');
        $this->social_template = $this->validateUrl(TEMPLATE_PATH.'helpers/social.ctp');
        $this->main_menu_template = $this->validateUrl(TEMPLATE_PATH.'helpers/main_menu.ctp');
        $this->bottom_menu_template = $this->validateUrl(TEMPLATE_PATH.'helpers/bottom_menu.ctp');
        $this->banner_top_template = $this->validateUrl(TEMPLATE_PATH.'helpers/banner_top.ctp');
        $this->footer_template = $this->validateUrl(TEMPLATE_PATH.'helpers/footer.ctp');
    }

    protected function validateUrl($url){
        if(file_exists($url)){
            return $url;
        } else{
            die('Template boot error: no such template ('.$url.')');
        }
    }
}