<?php
abstract class Controller{
    protected $params;
    protected $model;
    protected $view;

    public abstract function index();

    public function __construct($params){
        $this->params = $params;
        $this->init();
    }

    private function init(){
        $model_name = ucfirst($this->params['controller']).'Model';
        $view_name =  ucfirst($this->params['controller']).'View';
        $this->model = new $model_name($this->params);
        $this->view = new $view_name($this->params);
    }
}