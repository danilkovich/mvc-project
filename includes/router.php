<?php
class Router{
    private $params = array(
        'controller' => 'main',
        'action' => 'index',
    );

    public function getPage(){
        if($_SESSION){
            session_start();
        }
        $this->getController();
    }

    private function getController(){
        if($_GET){
            foreach($_GET as $item => $value){
                $this->params[$item] = DB::real_escape_string(strtolower($value));
            }
        }

        $mvc_url = array(
            $model_path = MODELS_PATH."{$this->params['controller']}.model.php",
            $view_path = VIEWS_PATH."{$this->params['controller']}.view.php",
            $controller_path = CONTROLLERS_PATH."{$this->params['controller']}.controller.php",
        );
        /*
         * Подключаем контроллер, вызывыем action
         */
        foreach($mvc_url as $file){
            if(file_exists($file)){
                require_once($file);
            } else{
                die('No such file exists');
            }
        }

        $controller_name = ucfirst($this->params['controller']).'Controller';
        $controller = new $controller_name($this->params);

        if(method_exists($controller,$this->params['action'])){
            call_user_func(array($controller,$this->params['action']));
        }
        else{
            die("No {$this->params['action']}() method exists in class {$controller_name}");
        }
    }
}