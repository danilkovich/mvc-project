<?php
define('TEMPLATE_PATH', $_SERVER['DOCUMENT_ROOT'].'/templates/');
define('CONTROLLERS_PATH', $_SERVER['DOCUMENT_ROOT'].'/controllers/');
define('MODELS_PATH', $_SERVER['DOCUMENT_ROOT'].'/models/');
define('IMG_DATA', $_SERVER['DOCUMENT_ROOT'].'/img/');
define('FORUM_PATH', $_SERVER['DOCUMENT_ROOT'].'/forum/');
define('HELPERS_PATH', $_SERVER['DOCUMENT_ROOT'].'/helpers/');
define('VIEWS_PATH', $_SERVER['DOCUMENT_ROOT'].'/views/');
define('NEWS_IMG_DATA', 'http://'.$_SERVER['HTTP_HOST'].'/upload/images/');