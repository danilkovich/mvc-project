<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/includes/constants.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/includes/db_connection.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/includes/router.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/includes/core/model.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/includes/core/view.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/includes/core/controller.php');
