<?php
class Db{
    private static $db_hostname = 'localhost';
    private static $db_database = 'mvcpr_db';
    private static $db_username = 'root';
    private static $db_password = '';
    private static $link;

    public static function query($query){
        self::check_connection();
        $result = mysqli_query(self::$link,$query);
        $content = array();

        if(self::getError()){
            die(self::getError());
        }

        if(!is_bool($result)){
            while($row = mysqli_fetch_assoc($result)){
                $content[] = $row;
            }
            return $content;
        }
        return $result;
    }

    public static function real_escape_string($string){
        self::check_connection();

        return mysqli_real_escape_string(self::$link,$string);
    }

    private static function check_connection(){
        if(!self::$link){
            self::$link = mysqli_connect(self::$db_hostname,self::$db_username,self::$db_password,self::$db_database);

            if(mysqli_error(self::$link)){
                die('Connection Error ('.mysqli_errno(self::$link).') '.mysqli_error(self::$link));
            }

           mysqli_set_charset(self::$link,'utf8');
        }
    }

    public static function get_id(){
        return mysqli_insert_id(self::$link);
    }

    public static function get_date(){
        $query = "select curdate()";
        return self::query($query);
    }

    public static function getError(){
        return mysqli_error(self::$link);
    }
}