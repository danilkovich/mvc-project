<?php
class MainView extends View{
    protected $header_template;
    protected $sidebar_template;

    public function __construct($params){
        parent::__construct($params);
        $this->header_template = $this->validateUrl($_SERVER['DOCUMENT_ROOT'].'/templates/helpers/header.ctp');
        $this->sidebar_template = $this->validateUrl($_SERVER['DOCUMENT_ROOT'].'/templates/helpers/sidebar.ctp');
    }

    public function show($data){
        include_once($this->index_template);
    }
}