<?php
class AuthView extends View{

    public function __construct($params){
        $this->alias = 'login';
        parent:: __construct($params);
    }

    public function show($data){
        include_once($this->page_template);
    }
}