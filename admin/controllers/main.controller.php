<?php
namespace Admin;
class MainController extends Controller{

    public function index(){
        $data = $this->model->getData();
        $this->view->show($data);
    }

    public function show(){
        $data = $this->model->getData();
        $this->view->show($data);
    }

    public function remove(){
        $this->model->remove();
    }
}