<?php
namespace Admin;
use Db;
class MainModel extends Model{
private $comment;

    public function getData(){
        $data['menu'] = $this->getMenu();
        $data['content'] = $this->{$this->params['action']}();
        $data['comment'] = $this->comment;
        return $data;
    }

    public function index(){
        if($_POST){
            $this->comment = $this->add();
        }
        $query = "select * from logos";
        return Db::query($query);
    }

    public function show(){
        if($_POST){
            $this->comment = $this->edit();
        }
        $query = "select title,image_title,url from logos where id='{$this->params['id']}'";
        if($result = Db::query($query)){
            $data = $this->fetchSingleRow($result);
            return $data;
        }
        die('Не удалось извлечь логотип: '.Db::getError());
    }

    public function add(){
        $upload_file = 'logo';
        $is_empty = $this->isEmptyPost();
        if($_FILES[$upload_file]['size'] !== 0 && !$is_empty){
            $upload = $this->uploadValidate();
            $upload_dir = $_SERVER['DOCUMENT_ROOT'].'/upload/logos';
            $uploaded_files = $this->filesUpload($upload_file,$upload_dir);
            foreach($uploaded_files as $value){
                $query = "insert into logos (title,image_title,url)
                          values ('{$upload['title']}','{$value}','{$upload['url']}')";
                if(!Db::query($query)){
                    $this->unlinkFile($uploaded_files,$upload_dir);
                    die(Db::getError());
                }
            }
            return 'Логотип успешно добавлен на сайт.';
        }
        return 'Логотип не был добавлен. Остались незаполненные поля!';
    }

    public function edit(){
        $is_empty = $this->isEmptyPost();
        if(!$is_empty){
            $upload = $this->uploadValidate();
            $query = "update logos set title ='{$upload['title']}',url='{$upload['url']}' where id='{$this->params['id']}'";
            if(Db::query($query)){
                $files = 'logo';
                if($_FILES[$files]['size'] !== 0){
                    $upload_dir = $_SERVER['DOCUMENT_ROOT'].'/upload/logos';
                    $uploaded_files = $this->filesUpload($files,$upload_dir);

                    $query = "select image_title from logos where id='{$this->params['id']}'";
                    $temp = $this->fetchSingleRow(Db::query($query));
                    if($temp){
                        foreach($uploaded_files as $value){
                            $query = "update logos set image_title='{$value}' where id='{$this->params['id']}'";
                            if(!Db::query($query)){
                                $this->unlinkFile($uploaded_files,$upload_dir);
                                return 'Не удалось обновить изобоажение: '.Db::getError();
                            }
                        }
                        $this->unlinkFile($temp,$upload_dir);
                        return 'Логотип успешно обновлён.';
                    }
                    $this->unlinkFile($uploaded_files, $upload_dir);
                    return 'Не удалось извлечь изображение: '.Db::getError();
                }
                return 'Логотип успешно обновлён.';
            }
            return 'Не удалось внести изменения: '.Db::getError();
        }
        return 'Заполнены не все поля.';
    }

    public function remove(){
        $dir = $_SERVER['DOCUMENT_ROOT'].'/upload/logos';
        $query = "select image_title from logos where id='{$this->params['id']}'";
        $data = $this->fetchSingleRow(Db::query($query));
        $this->unlinkFile($data,$dir);

        $query = "delete from logos where id='{$this->params['id']}'";
        if(Db::query($query)){
            header("Location: http://{$_SERVER['HTTP_HOST']}/admin/?controller=main&action=index");
            exit;
        }
        die('Логотип не был удалён: '.Db::getError());
    }
}