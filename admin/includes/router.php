<?php
namespace Admin;
use Db;
class Router{
    private $params = array(
        'controller' => 'main',
        'action' => 'index',
    );

    public function getPage(){
        session_start();
        if(!$_SESSION['admin']){
            header("Location: http://{$_SERVER['HTTP_HOST']}/?controller=auth&action=login");
            exit;
        }
        $this->getController();
    }

    private function getController(){
        if($_GET){
            foreach($_GET as $item => $value){
                $this->params[$item] = DB::real_escape_string(strtolower($value));
            }
        }

        $mvc = array(
            $model_path = "{$_SERVER['DOCUMENT_ROOT']}/admin/models/{$this->params['controller']}.model.php",
            $view_path = "{$_SERVER['DOCUMENT_ROOT']}/admin/views/{$this->params['controller']}.view.php",
            $controller_path = "{$_SERVER['DOCUMENT_ROOT']}/admin/controllers/{$this->params['controller']}.controller.php",
        );
        /*
         * Подключаем контроллер, вызывыем action
         */
        foreach($mvc as $file){
            if(file_exists($file)){
                require_once($file);
            } else{
                die('No such file exists: '.$file);
            }
        }
        $controller_name =  __NAMESPACE__.'\\'.ucfirst($this->params['controller']).'Controller';
        $controller = new $controller_name($this->params);

        if(method_exists($controller,$this->params['action'])){
            call_user_func(array($controller,$this->params['action']));
        }
        else{
            die("No {$this->params['action']}() method exists in class {$controller_name}");
        }
    }
}
