<?php
namespace Admin;
use DB;
abstract class Model{
    protected $params;

    public function __construct($params){
        $this->params = $params;
    }

    public abstract function getData();

    protected  function getMenu(){
        $menu = array(
            'Главная' => array(
                'url' => '/admin/?',
                'is_active' => false,
            ),
            'Страницы' => array(
                'url' => '/admin/?',
                'is_active' => false,
            ),
            'Статьи' => array(
                'url' => '/admin/?',
                'is_active' => false,
            ),
            'Партнёры' => array(
                'url' => '/admin/?',
                'is_active' => false,
            ),
            'Галерея' => array(
                'url' => '/admin/?',
                'is_active' => false,
            ),
            'Баннера' => array(
                'url' => '/admin/?',
                'is_active' => false,
            ),
        );

        $this->setUrl($menu);

        return $menu;
    }

    protected function setUrl(&$menu){
        foreach($menu as &$value){
            $value['url'] .= http_build_query(array(
                'controller' => $value['controller'],
                'action' => $value['action']));
            $value['is_active'] = ($this->params['controller'] == $value['controller']);
            unset($value);
        }
        unset($menu);
    }

    protected function filesUpload($upload_file,$upload_dir,$multiple=false){
        $uploaded_files = array();
        $this->fixDir($upload_dir);

        if($multiple){
            for($i=0; $i<count($_FILES[$upload_file]['name']); $i++) {
                $file_name = basename($_FILES[$upload_file]['name'][$i]);
                do{
                    $new_file_name = uniqid().strrchr($file_name,'.');
                }while(file_exists($upload_dir.'/'.$new_file_name));

                $tmp_file_path = $_FILES[$upload_file]['tmp_name'][$i];
                $new_file_path = $upload_dir.'/'.$new_file_name;

                if(move_uploaded_file($tmp_file_path, $new_file_path)){
                    chmod($new_file_path,0755);
                    $file_names = array($file_name => $new_file_name);
                    $uploaded_files += $file_names;
                }
                else{
                    $this->unlinkFile($uploaded_files,$upload_dir);
                    die('Error: Can\'t move uploaded files');
                }
            }
        }
        else{
            $file_name = basename($_FILES[$upload_file]['name']);
            do{
                $new_file_name = uniqid().strrchr($file_name,'.');
            }while(file_exists($upload_dir.'/'.$new_file_name));

            $tmp_file_path = $_FILES[$upload_file]['tmp_name'];
            $new_file_path = $upload_dir.'/'.$new_file_name;

            if(move_uploaded_file($tmp_file_path,$new_file_path)){
                chmod($new_file_path,0755);
                $file_names = array($file_name => $new_file_name);
                $uploaded_files += $file_names;
            }
            else{
                $this->unlinkFile($uploaded_files,$upload_dir);
                die('Error: Can\'t move uploaded files');
            }
        }
        return $uploaded_files;
    }

    protected function unlinkFile($file,$dir){
        if(is_array($file)){
            foreach($file as $value){
                $this->unlinkFile($value,$dir);
            }
        }
        $dir = rtrim($dir,'/');
        if(unlink($dir.'/'.$file)){
            if(count(scandir($dir)) <= 2){
                rmdir($dir);
            }
            return true;
        }
        return false;
    }

    protected function fixDir(&$dir){
        $dir = rtrim($dir,'/');
        if(!file_exists($dir) && !is_dir($dir)){
            if(!mkdir($dir,0755,true)){
                die('Error: Can\'t create new directory.');
            }
            chmod($dir,0777);
        }
    }

    protected function navList($count,$limit){
        $pages = $this->countPages($count,$limit);
        if($pages > 1){
            $nav_list = array();
            $cur_page = $this->getPage();
            $url_query = parse_url($_SERVER['REQUEST_URI'],PHP_URL_QUERY);
            $parse_url = isset($this->params['page']) ? substr($url_query,0,strrpos($url_query,'&')) : $url_query;
            $url ='http://'.$_SERVER['HTTP_HOST'].'/admin/?'.$parse_url;

            $prev = ($cur_page <= 1) ? '1' : ($cur_page -1);
            $next = ($cur_page >= $pages) ? $pages : ($cur_page +1);
            $start_page = (($cur_page - 3) > 2) ? ($cur_page - 3) : 2;
            $end_page = (($cur_page + 2) < ($pages - 1)) ? ($cur_page + 2) : ($pages - 1);

            if($cur_page > 1){
                $nav_list[] = array(
                    'title' => '<',
                    'url' => $url.'&page='.$prev,
                    'move' => true,
                );
            }

            if($pages > 5) {
                $nav_list[] = array(
                    'title' => '1',
                    'url' => $url.'&page=1',
                    'is_active' => ($cur_page == 1) ? true : false,
                );

                if($start_page != 2){
                    $nav_list[] = array(
                        'title' => '...',
                        'space' => true,
                    );
                }

                for ($i = $start_page; $i <= $end_page; $i++) {
                    $nav_list[] = array(
                        'title' => $i,
                        'url' => $url.'&page='.$i,
                        'is_active' => ($cur_page == $i) ? true : false,
                    );
                }

                if($end_page != $pages - 1){
                    $nav_list[] = array(
                        'title' => '...',
                        'space' => true,
                    );
                }

                $nav_list[] = array(
                    'title' => $pages,
                    'url' => $url.'&page='.$pages,
                    'is_active' => ($cur_page == $pages) ? true : false,
                );
            } else {
                for ($i = 1; $i <= $pages; $i++) {
                    $nav_list[] = array(
                        'title' => $i,
                        'url' => $url.'&page='.$i,
                        'is_active' => ($cur_page == $i) ? true : false,
                    );
                }
            }
            if($cur_page < $pages){
                $nav_list[] = array(
                    'title' => '>',
                    'url' => $url.'&page='.$next,
                    'move' => true,
                );
            }
            return $nav_list;
        }
        return null;
    }
    protected function countPages($elements,$limit){
        $pages = ($elements % $limit == 0)
            ? $elements/$limit
            : floor($elements/$limit) + 1;
        return $pages;
    }

    protected function getPage(){
        return isset($this->params['page']) ? $this->params['page'] : 1;
    }

    protected function getCategories(){
        $query = "select * from {$this->params['controller']}_categories";
        return Db::query($query);
    }

    protected function fetchSingleRow($data){
        if(is_array($data) && count($data) == 1){
            foreach($data as $value){
                $data = $this->fetchSingleRow($value);
            }
        }
        return $data;
    }

    protected function isEmptyPost(){
        $is_empty = true;
        foreach($_POST as $item){
            if($is_empty = empty($item)){
                break;
            }
        }
        return $is_empty;
    }

    protected function ksortTree( &$array )
    {
        if (!is_array($array)) {
            return false;
        }

        ksort($array);
        foreach ($array as $k=>$v) {
            $this->ksortTree($array[$k]);
        }
        return true;
    }

    protected function krsortTree( &$array )
    {
        if (!is_array($array)) {
            return false;
        }

        krsort($array);
        foreach ($array as $k=>$v) {
            $this->krsortTree($array[$k]);
        }
        return true;
    }

    protected  function uploadValidate(){
        $upload = array();
        foreach($_POST as $item => $value){
            $upload[$item] = Db::real_escape_string($value);
        }
        return $upload;
    }
}