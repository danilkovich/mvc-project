<?php
namespace Admin;
abstract class View{
    protected $params;
    protected $controller;
    protected $alias;
    protected $page_template;
    protected $index_template;
    protected $url_query;/*для html формы*/

    public abstract function show($data);

    public function __construct($params){
        $this->params = $params;
    }

    protected function init(){
        $params = $this->params;
        $this->controller = $params['controller'];
        $this->alias = !empty($params['alias']) ? $params['alias'] : $params['action'];
        $this->page_template = $this->validateUrl($_SERVER['DOCUMENT_ROOT'].'/admin/templates/'.$this->controller.'/'.$this->params['action'].'.ctp');
        $this->index_template = $this->validateUrl($_SERVER['DOCUMENT_ROOT'].'/admin/templates/index.ctp');
        $this->url_query = parse_url($_SERVER['REQUEST_URI'],PHP_URL_QUERY);
    }

    protected function validateUrl($url){
        if(file_exists($url)){
            return $url;
        } else{
            die('Template boot error: no such template ('.$url.')');
        }
    }
}