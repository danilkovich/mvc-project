<div class="main-menu">
    <div class="container">
        <ul id="drop-down">
            <?php
            /*
             * Отображаем меню сайта
             */
            foreach($data['bottom_menu'] as $item => $value){?>

                <li>
                    <a href="<?=$value['url']?>"target="_top" <?php if($value['is_active']){?>class="menu-item-active"<?php } ?>><?=$item?></a>
                    <?php
                    if(isset($value['drop_down'])){?>
                        <ul>
                            <?php foreach($value['drop_down'] as $drop_down_item => $drop_down_value){?>
                                <li>
                                    <a href="<?=$drop_down_value['url']?>"target="_top"><?=$drop_down_item?></a>
                                </li>
                            <?php } ?>
                        </ul>

                    <?php } ?>
                </li>
            <?php } ?>
        </ul>
    </div>
</div>