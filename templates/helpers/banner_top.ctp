<div class="banner-top">
    <ul>
        <?php foreach($data['banners']['top'] as $value){?>
            <?php if(!empty($value['image_title'])) {?>
                <li>
                    <a href="<?=$value['url']?>">
                        <img src="/upload/banners/<?=$value['image_title']?>"/>
                    </a>
                </li>
            <?php } ?>
        <?php } ?>
    </ul>
</div>