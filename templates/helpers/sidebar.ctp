<div class="sidebar">
    <div class="sidebar-banner sidebar-banner-top">
        <?php if($data['banners']['sidebar_top']['image_title'] != null) {?>
            <a href="<?=$data['banners']['sidebar_top']['url']?>">
                <img src="/upload/banners/<?=$data['banners']['sidebar_top']['image_title']?>"/>
            </a>
        <?php } ?>
    </div>
    <div class="sidebar-news">
        <p>
            Новини галузі
        </p>
        <?php
            foreach($data['sidebar'] as $value){?>
                <div class="sidebar-news-items">
                    <a href="http://<?=$_SERVER['HTTP_HOST']?>/?controller=articles&action=news&alias=item&id=<?=$value['id']?>">
                        <img src="/upload/news_img/<?=substr($value['date'],0,4)?>/<?=substr($value['date'],0,7)?>/<?=$value['date']?>/<?=$value['image_title']?>"/>
                        <div class="sidebar-items-date"><?=$value['date']?></div><?=$value['title']?>
                    </a>
                </div>
        <?php } ?>
    </div>
    <div class="sidebar-banner sidebar-banner-bottom">
        <?php if($data['banners']['sidebar_bottom']['image_title'] != null) {?>
            <a href="<?=$data['banners']['sidebar_bottom']['url']?>">
                <img src="/upload/banners/<?=$data['banners']['sidebar_bottom']['image_title']?>"/>
            </a>
        <?php } ?>
    </div>
</div>