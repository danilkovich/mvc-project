<div class="main-menu">
    <div class="container">
        <ul id="drop-down">
            <?php
            /*
             * Отображаем меню сайта
             */
            foreach($data['main_menu'] as $item => $value){?>

                <li>
                <a href="<?=$value['url']?>"target="_top" <?php if($value['is_active']){?>class="menu-item-active"<?php } ?>><?=$item?></a>
                <?php
                if(isset($value['drop_down'])){?>
                    <ul>
                        <?php foreach($value['drop_down'] as $drop_down_item => $drop_down_value){?>
                            <li>
                                <a href="<?=$drop_down_value['url']?>"target="_top"><?=$drop_down_item?></a>
                            </li>
                        <?php } ?>
                    </ul>

                <?php } ?>
                </li>
            <?php } ?>
        </ul>
    </div>
</div>

<script type="text/javascript" charset="utf-8">
    $(function(){
        $('#drop-down li').hover(function(){
            $(this).children('ul').stop(false,true).fadeIn(300);
        },function(){
            $(this).children('ul').stop(false,true).fadeOut(300);
        });
    });
</script>
