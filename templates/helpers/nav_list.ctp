<div class="nav-list">
    <ul>
        <?php
        foreach($data['content']['nav_list'] as $value){?>
            <li>
                <?php if($value['space']){ ?>
                    <span class="nav-list-space"><?=$value['title']?></span>
                <?php } else{ ?>
                    <a href="<?=$value['url']?>"
                       <?php if($value['move']){?>class="nav-list-move" <?php }
                        else if($value['is_active']){ ?>class="menu-item-active"<?php } ?>>
                        <?=$value['title']?>
                    </a>
                <?php } ?>
            </li>
        <?php } ?>
    </ul>
</div>