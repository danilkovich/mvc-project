<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8"/>
    <title>Логин</title>
    <link type="text/css" rel="stylesheet" href="/styles/css/login.css"/>
</head>
<body>
<?php
if(!empty($data['warning'])){
    echo "<span>{$data['warning']}</span>";
}
?>
<form method="POST" action="/?controller=auth&action=login">
    <table class="table">
        <tr>
            <td>Логин <input type="text" name="login" placeholder="Введите свой логин"/></td>
        </tr>
        <tr>
            <td>Пароль <input type="password" name="password" placeholder="Введите свой пароль"/></td>
        </tr>
        <tr>
            <td><input type="submit" value="Войти"/></td>
        </tr>
    </table>
</form>
</body>
</html>