<div class="logo-list">
    <?php foreach($data['content'] as $value){ ?>
        <div class="logo-element">
            <a href="<?=$value['url']?>">
                <img src="/upload/logos/<?=$value['image_title']?>"/>
            </a>
        </div>
    <?php } ?>
</div>