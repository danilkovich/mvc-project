<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8"/>

    <link rel="icon" href="/styles/img/favicon.ico" type="image/x-icon"/>
    <link rel="shortcut icon" href="/styles/img/favicon.ico" type="image/x-icon"/>

    <title>
        <?=$data['site_name']?>
    </title>
    <link type="text/css" rel="stylesheet" href="/styles/css/style.css"/>
    <link type="text/css" rel="stylesheet" href="/styles/css/lightbox.css"/>
    <!-- jQuery -->
    <script src="/styles/js/jquery.js"></script>
    <script src="/styles/js/plugins/lightbox/lightbox.min.js"></script>
    <script src="/styles/js/plugins/chained/chained.js"></script>
</head>
<body>
    <div class="container">
        <?php
            include_once($this->logo_template);
            include_once($this->social_template);
        session_start();
        if($_SESSION['admin']){?>
            <div class="login">
                <a href="admin/">Админ панель</a>
            </div>
        <?php } ?>
    </div>
    <?php
        include_once($this->main_menu_template);

        if(isset($this->header_template)){
            include_once($this->header_template);
        }

        include_once($this->bottom_menu_template);
    ?>

    <div class="container">
        <?php
            include_once($this->banner_top_template);

            if(isset($this->sidebar_template)) {
                include_once($this->sidebar_template);
            }
            ?>
        <div class="content <?php if(isset($this->sidebar_template)){ ?> content-margin <?php } ?>">
            <?php
                include_once($this->page_template);

                if(isset($this->nav_list_template)) {
                    include_once($this->nav_list_template);
                }
            ?>
        </div>
    </div>

    <div id="footer">
        <?php
            include_once($this->footer_template);
        ?>
    </div>
</body>
</html>

